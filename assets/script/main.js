/* *****=== Reusable Function Start ===***** */

// Function to toggle the ScrollBar
const toggleScrollBar = () => {
    document.body.classList.toggle("no-scroll");
}

/* *****=== Reusable Function End   ===***** */


/*******************************************************************/


/*===== Navigation Events Start =====*/
document.addEventListener("DOMContentLoaded", () => {
    const links = document.querySelectorAll(".navigation__link");
    const checkbox = document.querySelector(".navigation__checkbox");

    // Function for Selecting a Link
    links.forEach( (link) => {
        link.addEventListener("click", () => {
            checkbox.checked = false; // Uncheck the checkbox when a link is clicked
            document.body.classList.remove("no-scroll"); // Removing the No-Scroll when a link is clicked
        });
    });
    
    /* Event Disabling the Scroll Behavior */
    // Toggle ScrollBar when checkbox is clicked
    checkbox.addEventListener("change", toggleScrollBar);

    // Close Nav when ESC key is pressed
    document.addEventListener("keydown", e => {
        if(e.key === "Escape" && checkbox.checked) {
            checkbox.checked = false;
            toggleScrollBar();
        }
    });
});
/*===== Navigation Events End   =====*/


/*******************************************************************/

/*===== POPUP Events Start =====*/
document.addEventListener("DOMContentLoaded", () => {
    const popup = document.getElementById("popup");
    const popupContainer = document.querySelector(".popup__container");
    const popupContent = document.querySelector(".popup__content");
    const btnShowPopup = document.querySelectorAll(".btn--white");
    const btnX = document.querySelector(".popup__close");
    const btnBook = document.getElementById("btn-book");
    const btnDiscover = document.querySelector(".btn--animated");

    // Function for Opening a Popup and Disabling the Scrollbar
    const openPopup = () => {
        popup.classList.remove("hidden");
        popupContainer.classList.remove("hidden");
        toggleScrollBar(); // Disable scroll when popup is open
    }

    // Function for CLosing a Popup and Enabling scrollbar
    const closePopup = () => {
        popup.classList.add("hidden");
        popupContainer.classList.add("hidden");
        // toggleScrollBar(); // Enable scroll when popup is closed
        document.body.classList.remove("no-scroll"); // removing the no-scroll class
    }

    // Show the popup
    // Accessing an array of btnShowPopup
    for(let i = 0; i < btnShowPopup.length; i++){
        btnShowPopup[i].addEventListener("click", openPopup);
    }

    // Prevent closing the popup when clicking on the popup content
    popupContent.addEventListener("click", e => {
        e.stopPropagation();
    });

    // Closing the popup when clicking outside the popup content
    popup.addEventListener("click", e => {
        if (e.target === popup) {
            closePopup();
        }
    });

    // Closing the Popup when "X" button is clicked and enabling the scrollbar
    btnX.addEventListener("click", e => {
        closePopup();
    });

    // Closing the Popup when "Book now" button is clicked and enabling the scrollbar
    btnBook.addEventListener("click", e => {
        closePopup();
    });

    // Prevent applying the "no-scroll or disabling the scrollbar" when "Discover our tours" button is clicked in the header
    btnDiscover.addEventListener("click", () => {
        // if (popup.classList.contains("hidden")) {
        //     toggleScrollBar();
        // }
        toggleScrollBar();
    });

    // Event for CLosing A Popup when "ESC" key is pressed
    document.addEventListener("keydown", e => {
        // if "Escape" key is pressed & popup is "Active"
        if(e.key === "Escape" && !popup.classList.contains("hidden")){
            closePopup();
        }
    });
});
/*===== POPUP Events End   =====*/